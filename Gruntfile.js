module.exports = function(grunt) {
    grunt.initConfig({
        postcss: {
            options: {
                processors: [
                    require('pixrem'),
                    require('autoprefixer')({browsers: ['> 0%']}),
                    require('postcss-flexboxfixer'),
                    require('cssnano')({autoprefixer:false,zindex: false})
                ]
            },
            soco: {
                files: [{
                    expand:true,
                    flatten:true,
                    src: ['scss/*.css', '!**/_vars.css'],
                    dest:'css/'
                }]
            }
        },
        watch: {
            css: {
                files: [
                    'scss/*.css'
                ],
                tasks:['postcss:soco']
            }
        }
    });
    grunt.loadNpmTasks('grunt-postcss');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.registerTask('default', ["watch"]);
    grunt.registerTask('css', ["postcss:soco"]);
};