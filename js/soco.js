(function ($) {
  $(window).on("qodeAjaxPageLoad", function () {
    setTimeout(function () {
      $('.wpb_animate_when_almost_visible').removeClass('wpb_animate_when_almost_visible');
      twinAccordions();
      fixFlex();
    });
    //  if (location.pathname === '/') {location.reload()};
  });
  $(window).on('load', twinAccordions);

  function twinAccordions() {
    $('#accordions').on("click", '.qode-title-holder', function (event) {
      if (event.originalEvent && event.originalEvent.isTrusted) {
        $(event.currentTarget).addClass('skipped');
      }
      $('.qode-title-holder.ui-state-active:not(.skipped)', $('#accordions')).trigger('click');
      setTimeout(function () {
        $('.skipped').removeClass('skipped');
      }, 123)
    });
  }

  function fixFlex (){
    $('img', '.leftCol').each(function(i,e){
      $(this).css({
        visibility:'hidden'
      });
      $(this).closest('.leftCol > .vc_column-inner').css({
        background:'url('+$(e).attr('src') +') no-repeat 50% 50% /cover'
      });
    })
  }

  $(window).on('load', function () {
    $('.logo_wrapper a').on('click', function () {
      $('#back_to_top').trigger('click');
      setTimeout(function () {
        if (location.pathname !== '/') {
          $('.content_inner').animate({opacity: 0}, 600);
        }
      }, 1000)
    });
    $('[href="https://online.tm2app.com/socoperformance"]').attr({
      target:'_blank'
    });
    fixFlex();
  })

})(jQuery);