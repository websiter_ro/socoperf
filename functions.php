<?php

// enqueue the child theme stylesheet

Function soco_enqueue_scripts() {
	wp_register_style( 'soco-style', get_stylesheet_directory_uri() . '/css/soco.css?v='.time());
	wp_enqueue_style( 'soco-style', ['stylesheet'] );
	wp_enqueue_script( 'soco-general', get_stylesheet_directory_uri() . '/js/soco.js?v='.time(), array( 'jquery' ) );
	if (function_exists('gravity_form_enqueue_scripts')) {
		gravity_form_enqueue_scripts(1);
	}
}

add_action( 'wp_enqueue_scripts', 'soco_enqueue_scripts', 999 );

register_nav_menus ([
	'home-menu' => 'Home Menu',
]);

function register_childpage_sidebar() {
	register_sidebar( array(
		'name'          => __( 'Siblings sidebar', 'wp' ),
		'id'            => 'childpage-sidebar',
		'description'   => __( 'Widgets in this area will show on subpages.', 'wp' ),
		'before_widget' => '<li id="%1$s" class="widget %2$s">',
		'after_widget'  => '</li>',
		'before_title'  => '<h2 class="widgettitle hide-me">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'register_childpage_sidebar' );